use std::{
    fmt,
    process::{Command, Output},
};

use anyhow::{anyhow, Context, Result};
use cfg_if::cfg_if;
use clap::{error::ErrorKind::ArgumentConflict, CommandFactory, Parser};
use command_group::CommandGroup;

cfg_if! {
    if #[cfg(not(target_os = "linux"))] {
        compile_error!("reprio only supports Linux");
    }
}

#[derive(Copy, Clone)]
enum Id {
    Pid(u32),
    Pgid(u32),
}
impl Id {
    fn pid(id: u32) -> Self {
        Self::Pid(id)
    }

    fn pgid(id: u32) -> Self {
        Self::Pgid(id)
    }

    fn format_renice_args(&self, args: &mut Vec<String>) {
        match self {
            Self::Pid(id) => args.extend([String::from("--pid"), id.to_string()]),
            Self::Pgid(id) => args.extend([String::from("--pgrp"), id.to_string()]),
        }
    }

    fn format_ionice_args(&self, args: &mut Vec<String>) {
        match self {
            Self::Pid(id) => args.extend([String::from("-p"), id.to_string()]),
            Self::Pgid(id) => args.extend([String::from("-P"), id.to_string()]),
        }
    }
}
impl fmt::Debug for Id {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Pid(id) => write!(f, "Pid({id})"),
            Self::Pgid(id) => write!(f, "Pgid({id})"),
        }
    }
}

fn main() -> Result<()> {
    let args = CLIArgs::parse();
    let high_prio = if !args.low { Some(args.highest) } else { None };

    // If we were passed in a list of IDs
    if let Ok(id) = args.ids[0].parse() {
        let create_id = if args.group { Id::pgid } else { Id::pid };

        let mut ids = vec![create_id(id)];
        for id in &args.ids[1..] {
            if let Ok(id) = id.parse() {
                ids.push(create_id(id));
            } else {
                CLIArgs::command()
                    .error(ArgumentConflict, "Process IDs and commands cannot be used together")
                    .exit();
            }
        }

        return set_priority(ids, high_prio);
    }

    // Otherwise run a command
    let pgid = Id::Pgid(
        Command::new(&args.ids[0])
            .args(&args.ids[1..])
            .group_spawn()
            .with_context(|| format!("Unable to spawn command: {}", args.ids.join(" ")))?
            .id(),
    );
    set_priority([pgid], high_prio)
}

fn set_priority(ids: impl IntoIterator<Item = Id>, high_prio: Option<i8>) -> Result<()> {
    for id in ids.into_iter() {
        set_cpu_prio(id, high_prio)?;
        set_io_prio(id, high_prio.is_some())?;
        eprintln!("Adjusted priority for {id:?}")
    }
    Ok(())
}

fn set_cpu_prio(id: Id, high_prio: Option<i8>) -> Result<()> {
    let cpu_prio = high_prio.unwrap_or(19);
    let mut args = vec![String::from("--priority"), cpu_prio.to_string()];
    id.format_renice_args(&mut args);

    validate_output(
        Command::new("/usr/bin/renice")
            .args(args)
            .output()
            .context("Unable to run renice command")?,
    )
}

fn set_io_prio(id: Id, high_prio: bool) -> Result<()> {
    let mut args = vec![String::from("-c")];
    if high_prio {
        args.extend(["2", "-n", "0"].map(String::from));
    } else {
        args.extend([String::from("3")]);
    }
    id.format_ionice_args(&mut args);

    validate_output(
        Command::new("/usr/bin/ionice")
            .args(args)
            .output()
            .context("Unable to run ionice command")?,
    )
}

fn validate_output(Output { status, stdout, stderr }: Output) -> Result<()> {
    if status.success() {
        return Ok(());
    }
    let exit_code = status.code().context("failed command, unable to get exit code")?;
    let stdout = String::from_utf8_lossy(&stdout[..]);
    let stderr = String::from_utf8_lossy(&stderr[..]);

    eprintln!("Failed command: exit={exit_code}");
    eprintln!("STDOUT:\n{stdout}");
    eprintln!("STDERR:\n{stderr}");

    Err(anyhow!("failed renice command"))
}

/// Sets a process or group of processes to either the highest (default) or lowest priority (CPU and IO).
///
/// Remember that process priority in Linux is its "niceness," i.e. the less nice (lower) the higher the priority.
///
/// This program assumes that `renice` and `ionice` are available in `/usr/bin/`.
#[derive(Parser)]
struct CLIArgs {
    /// Value to use for the highest priority
    #[arg(long, allow_hyphen_values = true, default_value = "-10")]
    highest: i8,

    /// Use the lowest priority
    #[arg(long)]
    low: bool,

    /// IDS are process group ids
    #[arg(short, long)]
    group: bool,

    /// IDs of the processes to adjust, can also be a command to run with priority
    #[arg(required = true)]
    ids: Vec<String>,
}
